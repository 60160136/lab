import java.util.*;

public class xo2 {

	static char[][] Table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static char turn = 'x';
	static char sum = 0;

	public static void ShowTable() {
		System.out.println("  1 2 3");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1 + " ");
			for (int j = 0; j < 3; j++) {
				System.out.print(Table[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void ShowWelcom() {
		System.out.println("Welcom to ox Game");
	}

	public static void ShowTurn() {
		if (turn == 'x') {
			System.out.print("");
		} else {
			System.out.print("");
		}
	}

	public static void SwitchTurn() {
		if (turn == 'x') {
			turn = 'o';
		} else {
			turn = 'x';
		}
	}

	public static void Input() {
		Scanner k = new Scanner(System.in);
		
			if (turn == 'x') {
				System.out.print("x Input row col : ");
				int row = k.nextInt();
				int col = k.nextInt();
			if(row<1 || row>3 || col<1 || col>3) {
				System.out.println("Please input again!!! ");
				System.out.print("x Input row col : ");
				row = k.nextInt();
				col = k.nextInt();
			}
			if(Table[row - 1][col - 1] == 'x' || Table[row - 1][col - 1] == 'o') {
				System.out.println("Can not input!!! ");
				System.out.print("x Input row col : ");
				row = k.nextInt();
				col = k.nextInt();
			}
			if (Table[row - 1][col - 1] == '-') {
				Table[row - 1][col - 1] = 'x';
				} 
		}
			
			if (turn == 'o') {
				System.out.print("o Input row col : ");
				int row = k.nextInt();
				int col = k.nextInt();
				if(row<1 || row>3 || col<1 || col>3) {
					System.out.println("Please input again!!! ");
					System.out.print("x Input row col : ");
					row = k.nextInt();
					col = k.nextInt();
				}
				if(Table[row - 1][col - 1] == 'x' || Table[row - 1][col - 1] == 'o') {
					System.out.println("Can not input!!! ");
					System.out.print("x Input row col : ");
					row = k.nextInt();
					col = k.nextInt();
				}
				if (Table[row - 1][col - 1] == '-') {
					Table[row - 1][col - 1] = 'o';
				}
		}
			sum++;
	}

	public static boolean CheckDraw() {
		if (sum >= 9) {
			System.out.print("Draw!!!");
			return true;
		} else {
			return false;
		}
	}

	public static boolean CheckXWin() {
		if (Table[0][0] == 'x' && Table[0][1] == 'x' && Table[0][2] == 'x') {
			return true;
		} else if (Table[1][0] == 'x' && (Table[1][1]) == 'x' && (Table[1][2]) == 'x') {
			return true;
		} else if (Table[2][0] == 'x' && Table[2][1] == 'x' && Table[2][2] == 'x') {
			return true;
		} else if (Table[0][0] == 'x' && Table[1][0] == 'x' && Table[2][0] == 'x') {
			return true;
		} else if (Table[0][1] == 'x' && Table[1][1] == 'x' && Table[2][1] == 'x') {
			return true;
		} else if (Table[0][2] == 'x' && Table[1][2] == 'x' && Table[2][2] == 'x') {
			return true;
		} else if (Table[0][0] == 'x' && Table[1][1] == 'x' && Table[2][2] == 'x') {
			return true;
		} else if (Table[0][2] == 'x' && Table[1][1] == 'x' && Table[2][0] == 'x') {
			return true;
		} else {
			return false;
		}

	}

	public static boolean CheckOWin() {
		if (Table[0][0] == 'o' && Table[0][1] == 'o' & Table[0][2] == 'o') {
			return true;
		} else if (Table[1][0] == 'o' && Table[1][1] == 'o' && Table[1][2] == 'o') {

			return true;
		} else if (Table[2][0] == 'o' && Table[2][1] == 'o' && Table[2][2] == 'o') {

			return true;
		} else if (Table[0][0] == 'o' && Table[1][0] == 'o' && Table[2][0] == 'o') {
			return true;
		} else if (Table[0][1] == 'o' && Table[1][1] == 'o' && Table[2][1] == 'o') {
			return true;
		} else if (Table[0][2] == 'o' && Table[1][2] == 'o' && Table[2][2] == 'o') {
			return true;
		} else if (Table[0][0] == 'o' && Table[1][1] == 'o' && Table[2][2] == 'o') {
			return true;
		} else if (Table[0][2] == 'o' && Table[1][1] == 'o' && Table[2][0] == 'o') {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {

		ShowWelcom();
		ShowTable();
		ShowTurn();
		for (;;) {

			if (turn == 'x') {
				Input();
				ShowTable();
				SwitchTurn();
				if (CheckDraw()) {
					break;
				}
				if (CheckXWin() == true) {
					System.out.print("x win!!!");
					break;
				}

			}
			if (turn == 'o') {
				Input();
				ShowTable();
				SwitchTurn();
				if (CheckDraw()) {
					break;
				}
				if (CheckOWin() == true) {
					System.out.print("o win!!!");
					break;
				}
			}
		}

	}

}
